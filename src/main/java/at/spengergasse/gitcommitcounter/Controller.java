package at.spengergasse.gitcommitcounter;

import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

@RestController
public class Controller {
    @Autowired
    BackgroundMethods methods;


    @GetMapping("/sync")
    @ResponseBody
    public String sync() throws Exception {
        LocalDateTime start = LocalDateTime.now();
        Integer count1 = methods.bruteforceCountCommitsSync("robbyrussell", "oh-my-zsh");
        Integer count2 = methods.bruteforceCountCommitsSync("libgdx", "libgdx-demo-superjumper");
        Integer count3 = methods.bruteforceCountCommitsSync("github-for-unity", "Unity");

        LocalDateTime end = LocalDateTime.now();

        long diff = ChronoUnit.SECONDS.between(start, end);

        return "<p>Start: " + start.toString() + " | End: " + end + " | Total: " + diff + " seconds</p>" +
                "<p>Count 1: " + count1.toString() + "</p>" +
                "<p>Count 2: " + count2.toString() + "</p>" +
                "<p>Count 3: " + count3.toString() + "</p>";
    }

    @GetMapping("/async")
    @ResponseBody
    public String async() throws Exception{
        LocalDateTime start = LocalDateTime.now();
        CompletableFuture<Integer> count1 =methods.bruteforceCountCommitsAsync("robbyrussell","oh-my-zsh");
        CompletableFuture<Integer> count2 =methods.bruteforceCountCommitsAsync("libgdx","libgdx-demo-superjumper");
        CompletableFuture<Integer> count3 =methods.bruteforceCountCommitsAsync("github-for-unity","Unity");
        CompletableFuture.allOf(count1,count2,count3).join();

        LocalDateTime end = LocalDateTime.now();

        long diff = ChronoUnit.SECONDS.between(start, end);

        return "<p>Start: " + start.toString() + " | End: " + end + " | Total: " + diff + " seconds</p>" +
                "<p>Count 1: " + count1.get().toString()+" CompletableFuture: "+ count1.toString() + "</p>" +
                "<p>Count 2: " + count2.get().toString()+" CompletableFuture: "+ count2.toString() + "</p>" +
                "<p>Count 3: " + count3.get().toString()+" CompletableFuture: "+ count3.toString() + "</p>";
    }
}
