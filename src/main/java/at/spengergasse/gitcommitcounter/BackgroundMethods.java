package at.spengergasse.gitcommitcounter;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.CompletableFuture;

@Service
public class BackgroundMethods {
    String targetBase = "https://api.github.com/repos/";
    String auth = "luki2036:f1ddd821fda8a006ea58838233bca704fef04d8d";
    byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(StandardCharsets.ISO_8859_1));
    String authHeader = "Basic "+ new String(encodedAuth);


    public Integer bruteforceCountCommitsSync(String owner, String repo) throws Exception{
        Integer commitcount = 0;
        JSONArray jsonArray = null;
        HttpClient client = HttpClientBuilder.create().build();
        JSONParser parser = new JSONParser();

        for (int x = 1; (commitcount == 0 || jsonArray.size()==30);x++){
            HttpGet request = new HttpGet(targetBase+""+owner+"/"+repo+"/commits?page="+x);
            request.setHeader(HttpHeaders.AUTHORIZATION, authHeader);

            HttpResponse response = client.execute(request);
            String jsonString = EntityUtils.toString(response.getEntity());

            if (jsonString.contains("{\"message\":\"API rate limit exceeded\""))
                return -1;

            jsonArray = (JSONArray) parser.parse(jsonString);
            commitcount+=jsonArray.size();
        }
        return commitcount;
    }

    @Async
    public CompletableFuture<Integer> bruteforceCountCommitsAsync(String owner, String repo)throws Exception{
        Integer commitcount = 0;
        JSONArray jsonArray = null;
        HttpClient client = HttpClientBuilder.create().build();
        JSONParser parser = new JSONParser();

        for (int x = 1; (commitcount == 0 || jsonArray.size()==30);x++){
            HttpGet request = new HttpGet(targetBase+""+owner+"/"+repo+"/commits?page="+x);
            request.setHeader(HttpHeaders.AUTHORIZATION, authHeader);

            HttpResponse response = client.execute(request);
            String jsonString = EntityUtils.toString(response.getEntity());

            if (jsonString.contains("{\"message\":\"API rate limit exceeded\""))
                throw new Exception("Request Limit reached!!!");

            jsonArray = (JSONArray) parser.parse(jsonString);
            commitcount+=jsonArray.size();
        }
        return CompletableFuture.completedFuture(commitcount);
    }
}
