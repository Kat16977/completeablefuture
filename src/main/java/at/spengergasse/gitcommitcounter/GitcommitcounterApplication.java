package at.spengergasse.gitcommitcounter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitcommitcounterApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitcommitcounterApplication.class, args);
    }

}
